package repository

import (
	"github.com/jmoiron/sqlx"
	"github.com/yuri-potatoq/zenvia-integration/models"
)

type ReponsePersistenceRepository struct {
	DB *sqlx.DB
}

func (rr ReponsePersistenceRepository) Insert(r *models.ResponsePersistence) error {
	_, err := rr.DB.NamedExec(`
        INSERT INTO response_persistence 
            (meta_fk, message_id, type, text, receiver, sender, error_code, error_message)
        VALUES 
            (:meta_fk, :message_id, :type, :text, :receiver, :sender, :error_code, :error_message)
    `, &r)

	if err != nil {
		return err
	}

	return nil
}
