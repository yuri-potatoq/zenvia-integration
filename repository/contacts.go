package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

type ContactsRepository struct {
	DB *sqlx.DB
}

func (cr *ContactsRepository) ContactFromName(name string) (contact string) {

	err := cr.DB.Get(&contact, `
	SELECT 
		"public"."mk_pessoas"."fone01" AS "fone01"
	FROM 
		"public"."mk_pessoas"
	WHERE 
		("public"."mk_pessoas"."nome_razaosocial" = $1)
	`, name)

	if err != nil {
		fmt.Printf("query execute error: %s", err)
		return ""
	}

	return
}
