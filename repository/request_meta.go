package repository

import (
	"github.com/jmoiron/sqlx"
	"github.com/yuri-potatoq/zenvia-integration/models"
)

type RequestMetaRepository struct {
	DB *sqlx.DB
}

func (rr RequestMetaRepository) Insert(r *models.RequestMeta) (session_id string, err error) {
	stmt, err := rr.DB.Preparex(`
		INSERT INTO request_metadata
			(preferred_username, to_send)
		VALUES 
			($1, $2)
		RETURNING session_id
	`)

	if err != nil {
		return "", err
	}

	/* insert data got and return inserted tuple */
	if err = stmt.QueryRow(
		r.PreferredUsername,
		r.ToSend,
	).Scan(&session_id); err != nil {
		return "", err
	}

	return session_id, nil
}

type SendStatus struct {
	SendCount         int    `db:"send_count"`
	MetaId            int    `db:"meta_id"`
	SessionId         string `db:"session_id"`
	PreferredUsername string `db:"preferred_username"`
	ToSend            int    `db:"to_send"`
	RequestTime       string `db:"request_time"`
}

func (rr RequestMetaRepository) ConsumeBySession(session_id string, d *SendStatus) (err error) {

	if err = rr.DB.Get(d, `
		SELECT
			(
				SELECT
					COUNT(*)
				FROM response_persistence 
					WHERE meta_fk=$1
			) AS send_count
			, *
		FROM request_metadata
			WHERE session_id=$1
	;
	`,
		session_id,
	); err != nil {
		return err
	}

	return nil
}
