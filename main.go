package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/yuri-potatoq/zenvia-integration/client"
	"github.com/yuri-potatoq/zenvia-integration/models"
	pb "github.com/yuri-potatoq/zenvia-integration/protos/auth_token"
	"github.com/yuri-potatoq/zenvia-integration/repository"
	"github.com/yuri-potatoq/zenvia-integration/utils"
)

var (
	httpPort = flag.String("p", "7080", "HTTP PORT")
)

func main() {

	// route handlers init
	r := mux.NewRouter()

	s := r.PathPrefix("/sms").Subrouter()

	db, err := NewDatabase("host=db user=docker password=docker dbname=postgres sslmode=disable")

	if err != nil {
		log.Fatalln("error opening the database:", err)
	}

	s.HandleFunc(
		"/send",
		// wrapper http handler with needed middlewares
		DatabaseMiddleware(db, AuthMiddleware(MassiveSend)),
	)

	s.HandleFunc(
		"/status/{session_id}",
		DatabaseMiddleware(db, RequireStatus),
	)

	srv := &http.Server{
		Handler:      r,
		Addr:         strings.Join([]string{"0.0.0.0", *httpPort}, ":"),
		WriteTimeout: 300 * time.Second,
		ReadTimeout:  300 * time.Second,
	}

	log.Printf("Server running at %s\n", *httpPort)

	if err := srv.ListenAndServe(); err != nil {
		log.Fatalln("error opening http server:", err)
	}

}

func AuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// fmt.Printf("ip: %s\n", r.Header.Get("X-Real-IP"))
		// fmt.Printf("ip: %s\n", r.Header.Get("X-Forwarded-For"))

		// for k, v := range r.Header {
		// 	fmt.Printf("k: %s v: %s\n", k, v)
		// }

		log.Println("Auth middleware handled!")

		// token := r.URL.Query().Get("token")

		// call auth gRPC server and handle response
		// auth := client.AuthRPC{Token: token}

		// if err := auth.AuthToken(); err != nil {
		// 	log.Printf("Token não autorizado err: %s", err.Error())

		// 	http.Error(rw, "", http.StatusNotFound)
		// 	json.NewEncoder(rw).Encode(client.GenericResponse{
		// 		Status:  "Error",
		// 		Message: "Token authentication has fail",
		// 	})

		// 	return
		// }

		ctx := r.Context()

		next.ServeHTTP(rw, r.WithContext(context.WithValue(ctx, "auth", "auth.TokenData")))
	}
}

func DatabaseMiddleware(db *sqlx.DB, next http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if err := db.Ping(); err != nil {
			log.Fatalln("error pinging database:", err)
		}

		log.Println("database has been connected")

		ctx := r.Context()

		next.ServeHTTP(rw, r.WithContext(context.WithValue(ctx, "db", db)))
	}
}

func RequireStatus(rw http.ResponseWriter, r *http.Request) {
	session_id := mux.Vars(r)["session_id"]

	db := r.Context().Value("db").(*sqlx.DB)

	rp := repository.RequestMetaRepository{DB: db}

	data := &repository.SendStatus{}

	err := rp.ConsumeBySession(session_id, data)

	if err != nil {
		log.Printf("consume error: %s", err)
	}

	json.NewEncoder(rw).Encode(data)
}

func MassiveSend(rw http.ResponseWriter, r *http.Request) {
	// thats will avoid CORS errors
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	// handle file from request
	f, _, err := r.FormFile("csv")

	if err != nil {
		fmt.Println("Arquivo não encontrado")
		http.Error(rw, "", http.StatusNotFound)
		json.NewEncoder(rw).Encode(client.GenericResponse{
			Status:  "Error",
			Message: "File not found in request",
		})

		return
	}

	defer f.Close()

	// got db from context which init that
	db := r.Context().Value("db").(*sqlx.DB)

	// send dependencies
	text := r.FormValue("text")
	from := r.FormValue("from")

	// userDt := r.Context().Value("auth").(*pb.UserData)
	userDt := pb.UserData{}

	// repository type
	rp := repository.ReponsePersistenceRepository{DB: db}
	m_rp := repository.RequestMetaRepository{DB: db}

	session_id, err := m_rp.Insert(&models.RequestMeta{
		PreferredUsername: userDt.GetPreferredUsername(),
	})

	if err != nil {
		log.Println(err)
	}

	// create csv reader
	reader := utils.CSVReader{
		Reader:    csv.NewReader(f),
		Separator: ';',
	}

	cli := client.ClientSession{
		Client: &http.Client{
			Timeout: time.Duration(0), // no timeout by assign 0
		},
		Url:   client.Url_V2,
		Token: os.Getenv("ZENVIA_TOKEN"),
	}
	
	// max chunk lenght to avoid issues on zenvia api
	chunk := 200

	go func() {
		for {
			err = SendManagment(chunk, &reader, &rp,
				// anonymous function that will be launch on the background
				func(to string, ch chan *models.ResponsePersistence, d int) {

					resp, er := cli.MakeRequest(
						client.SendBody{
							From: from,
							To:   to,
							Contents: []client.SendContent{
								{
									Type: "text",
									Text: text,
								},
							},
						},
					)

					if er != (client.ErrorBody{}) {
						log.Printf("response err:\t%+v\n", er)

						// when response contains an error
						ch <- &models.ResponsePersistence{
							MetaFk:       session_id,
							Sender:       from,
							Receiver:     to,
							ErrorCode:    er.Code,
							ErrorMessage: er.Message,
						}

					} else {
						log.Printf("response ok:\t%+v\n", resp)

						typ, text := func() (t string, txt string) {
							// unpack `Contents` type
							if len(resp.Contents) > 0 {
								t = resp.Contents[0].Type
								txt = resp.Contents[0].Text
							}
							return
						}()

						// when response its ok
						ch <- &models.ResponsePersistence{
							MetaFk:    session_id,
							Sender:    from,
							Receiver:  to,
							MessageId: resp.Id,
							Type:      typ,
							Text:      text,
						}
					}
				})

			if err != nil {
				log.Printf("finish err %s\n", err)
				return
			}

			fmt.Println("Next chunk sleeping...")
			time.Sleep(time.Duration(5) * time.Minute)
		}
	}()

	json.NewEncoder(rw).Encode(struct {
		Message   string `json:"message"`
		SessionId string `json:"session_id"`
	}{
		Message:   "finished",
		SessionId: session_id,
	})
}

type RequestCallback func(string, chan *models.ResponsePersistence, int)

func SendManagment(chunk int, r *utils.CSVReader, rp *repository.ReponsePersistenceRepository, rcb RequestCallback) (err error) {
	ch := make(chan *models.ResponsePersistence, chunk-1)

	// mk database connection
	mk, err := MkDatabase()

	if err != nil {
		log.Fatalf("Erro ao conectar com banco MK: %s", err)
	}

	mk_rp := repository.ContactsRepository{DB: mk}

	// scheduler request loop
	for counter := chunk; counter > 0; counter-- {
		to, err := r.Read()
		if err != nil {
			log.Println("EOF LINE")
			break
		}

		contact := mk_rp.ContactFromName(to)

		go rcb(contact, ch, counter)
	}

	// until request be send that channel handle response
	// and insert on database.
	for d := range ch {
		if err := rp.Insert(d); err != nil {
			fmt.Printf("err on db insert: %s\n", err)
		}
		log.Printf("inserted n: %+v", d)
	}

	if err != nil {
		log.Println("EOF LINE ERR")
		return
	}

	return nil
}
