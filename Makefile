PROTOC = $(shell which protoc)

PROJECT = sms-sender

OUT_PATH = protos/

OUT_FLAGS = --go_out=.
OUT_FLAGS += --go_opt=paths=source_relative
OUT_FLAGS += --go-grpc_out=.
OUT_FLAGS += --go-grpc_opt=paths=source_relative

PROTO_FILES = $(wildcard $(OUT_PATH)*/*.proto)

# PHONNY: init

# init: entry protos


proto:
	@echo "Making..."
	@$(PROTOC) $(OUT_FLAGS) $(PROTO_FILES)
	@echo "Proto files are creted!"

build: proto
	@CGO_ENABLED=0 go build -o $(PROJECT)


run: proto
	@go run .

clean: 
	rm -f $(PROJECT)
	rm -f protos/*/*.pb.go