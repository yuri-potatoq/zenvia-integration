module github.com/yuri-potatoq/zenvia-integration

go 1.16

require (
    github.com/gorilla/mux v1.8.0 // indirect
    github.com/jmoiron/sqlx v1.3.4 // indirect
    github.com/lib/pq v1.10.2 // indirect
    golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985 // indirect
    golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
    google.golang.org/genproto v0.0.0-20210729151513-df9385d47c1b // indirect
    google.golang.org/grpc v1.39.0 // indirect
    google.golang.org/protobuf v1.27.1 // indirect
)
