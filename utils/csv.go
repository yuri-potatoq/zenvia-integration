package utils

import (
	"encoding/csv"
	"sync"
)

type CSVReader struct {
	Reader    *csv.Reader
	Separator rune
	mtx       sync.Mutex
}

func (cr *CSVReader) Read() (s string, err error) {
	cr.mtx.Lock()

	defer cr.mtx.Unlock()

	cr.Reader.Comma = cr.Separator

	tuple, err := cr.Reader.Read()

	if err != nil {
		return "", err
	}

	return tuple[0], nil
}
