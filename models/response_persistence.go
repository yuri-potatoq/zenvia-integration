package models

import "time"

type ResponsePersistence struct {
	Id           int           `db:"id"`
	MetaFk       string        `db:"meta_fk"`
	Sender       string        `db:"sender"`
	Receiver     string        `db:"receiver"`
	MessageId    string        `db:"message_id"`
	Type         string        `db:"type"`
	Text         string        `db:"text"`
	ErrorCode    string        `db:"error_code"`
	ErrorMessage string        `db:"error_message"`
	SendTime     time.Duration `db:"send_time"`
}
