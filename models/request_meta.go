package models

import "time"

type RequestMeta struct {
	MetaId            string        `db:"meta_id"`
	SessionId         string        `db:"session_id"`
	PreferredUsername string        `db:"preferred_username"`
	ToSend            int           `db:"to_send"`
	RequestTime       time.Duration `db:"request_time"`
}
