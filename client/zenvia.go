package client

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type ClientSession struct {
	Url    UrlAPI
	Token  string
	Client *http.Client
}

func checkErr(err error, desc string) bool {
	if err != nil {
		log.Printf("error {%s} on : %s", err, desc)
		return true
	}

	return false
}

func (cs *ClientSession) MakeRequest(body SendBody) (
	okBody AcceptedBody, errBody ErrorBody,
) {
	json_body, err := json.Marshal(body)

	if checkErr(err, "marshal body") {
		return
	}

	req, err := http.NewRequest(
		"POST", cs.Url.String(), bytes.NewBuffer(json_body))

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-API-TOKEN", cs.Token)

	if checkErr(err, "build request") {
		return
	}

	// configure client
	transCfg := &http.Transport{
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: true}, // ignore expired SSL certificates
		IdleConnTimeout:     0,
		TLSHandshakeTimeout: 0,
		MaxIdleConns:        0,
	}

	cs.Client.Transport = transCfg

	resp, err := cs.Client.Do(req)

	if checkErr(err, fmt.Sprintf("send request with body: %+v", body)) {
		return
	}

	decod := json.NewDecoder(resp.Body)

	if resp.StatusCode != 200 {
		err := decod.Decode(&errBody)

		// fmt.Println("error resp")
		// fmt.Printf("%+v", err)
		if checkErr(err, "decode ERR body") {
			return
		}

		return okBody, errBody
	}

	fmt.Println("resp ok")
	err = decod.Decode(&okBody)

	if checkErr(err, "decode OK body") {
		return
	}

	return okBody, ErrorBody{}
}
