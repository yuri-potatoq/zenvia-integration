package client

import (
	"context"
	"fmt"
	"log"
	"time"

	"errors"

	"google.golang.org/grpc"

	pb "github.com/yuri-potatoq/zenvia-integration/protos/auth_token"
)

var (
	address = "0.0.0.0:8800"
)

type AuthRPC struct {
	Token     string
	TokenData *pb.UserData
}

func (rpc *AuthRPC) AuthToken() (err error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		fmt.Printf("Dial not create %s", err)
		return err
	}

	c := pb.NewAuthClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	rpc.TokenData, err = c.DecryptionToken(ctx, &pb.Token{Data: rpc.Token})

	if err != nil {
		log.Printf("Authentication failed %s", err)
		return err
	}
	// check if userData is not null
	if rpc.TokenData == (&pb.UserData{}) {
		return errors.New("authentication failed")
	}

	return nil
}
