package client

import (
	"time"
)

type UrlAPI int

const (
	Url_V1 UrlAPI = iota
	Url_V2
)

func (u UrlAPI) String() string {
	return [...]string{
		"https://api.zenvia.com/v2/channels/sms/messages",
		"https://api.zenvia.com/v1/channels/sms/messages",
	}[u-1]
}

// send body

type SendContent struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type SendBody struct {
	From     string        `json:"from"`
	To       string        `json:"to"`
	Contents []SendContent `json:"contents"`
}

// response error body

// type Detail struct {
// 	Code    string `json:"code"`
// 	Path    string `json:"path"`
// 	Message string `json:"message"`
// }

type ErrorBody struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	// Details []Detail `json:"details"`
}

// reponse accepted body

type ResponseContent struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type AcceptedBody struct {
	Id        string            `json:"id"`
	From      string            `json:"from"`
	To        string            `json:"to"`
	Direction string            `json:"direction"`
	Channel   string            `json:"channel"`
	Contents  []ResponseContent `json:"contents"`
	Timestamp time.Time         `json:"timestamp"`
}

// generic http json response

type GenericResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
