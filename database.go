package main

import (
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

    CREATE TABLE IF NOT EXISTS request_metadata (
        meta_id SERIAL PRIMARY KEY
        ,session_id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4()
        ,preferred_username VARCHAR(100) NOT NULL
        ,to_send INTEGER
        ,request_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
    );

    CREATE TABLE IF NOT EXISTS response_persistence (
        id SERIAL PRIMARY KEY
        ,meta_fk uuid NOT NULL
        ,message_id VARCHAR(100)
        ,type VARCHAR(10)
        ,text VARCHAR(200)
        ,receiver VARCHAR(64)
        ,sender VARCHAR(64)
        ,error_code VARCHAR(100)
        ,error_message VARCHAR(100)
        ,send_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
    );
`

func NewDatabase(path string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("postgres", path)
	if err != nil {
		return db, err
	}

	db.MustExec(schema)

	return db, err
}

func MkDatabase() (*sqlx.DB, error) {
	connect_str := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("MK_HOST"),
		os.Getenv("MK_USER"),
		os.Getenv("MK_PASS"),
		os.Getenv("MK_DB"),
	)

	db, err := sqlx.Connect("postgres", connect_str)
	if err != nil {
		return db, err
	}

	return db, err
}
