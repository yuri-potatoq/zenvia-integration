# Zenvia Integration - SMS sender

```
At this project you can make HTTP request to application
and send a list of contacts in csv format thats are send
to destination after there being be authenticated.
```

## Routes

<br/>

**GET** `/sms/send`

```json
{
    "csv": "file.csv", 
    "text": "message to send!",
    "from": "DDDXXXXXXXXX"
}
```
`csv: File thats contain all contats to being send`

`from: Contact which be sender for all of messages`

## Test command

```sh
$ curl --location --request GET 'domain:7080/sms/send' \
--form 'csv=@"/path/to/file/file.csv"' \
--form 'text="message to send!"' \
--form 'from="91993543023"'
```

## Init sms container

```sh
# TO RUN ON DEPLOY

$ docker-compose build --build-arg ZENVIA_TOKEN=$TOKEN sms
$ docker-compose up -d sms

# OR

$ ZENVIA_TOKEN=XXXX-XXXX-XXXX-XXXX
$ docker-compose up -d --build

# OR

$ echo "ZENVIA_TOKEN=XXXX-XXXX-XXXX-XXXX" >> .env
$ source .env
$ docker-compose up -d --build

# TO RUN DEV CONTAINER

$ docker-compose run dev
# into container
$ apk add make protoc
$ export CGO_ENABLED=0
```

















