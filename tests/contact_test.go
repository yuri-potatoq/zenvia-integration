package testes

import (
	"fmt"
	"log"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/yuri-potatoq/zenvia-integration/repository"
)

func MkDatabase() (*sqlx.DB, error) {
	connect_str := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s sslmode=disable",
		// os.Getenv("MK_HOST"),
		// os.Getenv("MK_USER"),
		// os.Getenv("MK_PASS"),
		// os.Getenv("MK_DB"),
		"db",
		"docker",
		"docker",
		"postgres",
	)

	db, err := sqlx.Connect("postgres", connect_str)
	if err != nil {
		return db, err
	}

	return db, err
}

func TestTakeContact(t *testing.T) {
	db, err := MkDatabase()

	if err != nil {
		log.Fatalf("Erro ao conectar com banco MK: %s", err)
	}

	rp := repository.ContactsRepository{DB: db}

	name := "YURI"
	
	contact := (rp).ContactFromName(name)

	t.Logf("Contato %s para %s \n", contact, name)
	
}
