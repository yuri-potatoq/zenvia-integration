package testes

import (
	"encoding/csv"
	"fmt"
	"strings"
	"testing"

	"github.com/yuri-potatoq/zenvia-integration/utils"
)

func TestReadBuffer(t *testing.T) {
	separator := ';'

	want := func(s string) bool {
		return len(s) == 11
	}

	// readeble
	r := strings.NewReader(fmt.Sprintf("91993543023%d", separator))

	reader := utils.CSVReader{
		Separator: separator,
		Reader:    csv.NewReader(r),
	}

	if s, _ := reader.Read(); want(s) {
		t.Errorf("got: %s", s)
	}

}
