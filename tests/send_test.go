package testes

import (
	"net/http"
	"testing"

	"github.com/yuri-potatoq/zenvia-integration/client"
)

func TestSingleSend(t *testing.T) {
	want := client.ErrorBody{}

	cli := &client.ClientSession{
		Url:    client.Url_V2,
		Client: &http.Client{},
	}

	body := client.SendBody{
		From: "91993543023",
		To:   "91993543023",
		Contents: []client.SendContent{
			{
				Type: "text",
				Text: "text to send",
			},
		},
	}

	_, got := cli.MakeRequest(body)

	// thown error when got struct is empty
	if got == want {
		t.Errorf("\nwhen:\n\tbody: %+v\ngot: %+v", body, got)
	}
}

